<?php

namespace Jyrmo\WebService;

use Jyrmo\Router\ResponseInterface;

class ArrayResponse implements ResponseInterface {
	/**
	 * @var array
	 */
	protected $params = array();

	public function setParams(array $params = array()) {
		$this->params = $params;
	}

	public function setParam(string $key, $val) {
		$this->params[$key] = $val;
	}

    public function getParams() : array {
        return $this->params;
    }

	public function __construct(array $params = array()) {
		$this->setParams($params);
	}

	public function __toString() {
		$str = json_encode($this->params);

		return $str;
	}
}
