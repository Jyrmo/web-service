<?php

namespace Jyrmo\WebService;

interface MethodExtractorInterface {
    public function extract() : string;
}
