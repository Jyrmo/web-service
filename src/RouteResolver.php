<?php

namespace Jyrmo\WebService;

use Jyrmo\Router\{RouteResolverInterface, RequestInterface, RouteInterface};
use Jyrmo\ServiceManager\ServiceManagerInterface;

class RouteResolver implements RouteResolverInterface {
	/**
	 * @var array
	 */
	protected $routes = array();

	/**
	 * @var ServiceManagerInterface
	 */
	protected $serviceManager;

	public function setRoutes(array $routes) {
		$this->routes = $routes;
	}

	public function setServiceManager(ServiceManagerInterface $serviceManager) {
		$this->serviceManager = $serviceManager;
	}

	public function routeExists(Request $request) : bool {
		// TODO: separate lookups for key and method.

		$routeExists = false;
		$routeKey = $request->getRouteKey();
		if (isset($this->routes[$routeKey])) {
			$method = $request->getMethod();
			$routeExists = isset($this->routes[$routeKey][$method]);
		}

		return $routeExists;
	}

	public function resolve(RequestInterface $request) : RouteInterface {
        // TODO: request type check.

		$routeKey = $request->getRouteKey();
		$method = $request->getMethod();
		if (!$this->routeExists($request)) {
            // TODO: exception
		}
		$serviceName = $this->routes[$routeKey][$method];
		$route = $this->serviceManager->get($serviceName);

		return $route;
	}
}
