<?php

namespace Jyrmo\WebService;

interface ParamsExtractorInterface {
    public function extract() : array;
}
