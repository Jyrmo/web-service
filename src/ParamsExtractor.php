<?php

namespace Jyrmo\WebService;

class ParamsExtractor implements ParamsExtractorInterface {
    public function extract() : array {
        // TODO: more elegant method determination.

        $params = array();
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method === 'GET') {
            $params = $_GET;
        } elseif ($method === 'POST') {
            $params = $_POST;
        } else {
            // TODO: exception
        }

        return $params;
    }
}
