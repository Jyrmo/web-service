<?php

namespace Jyrmo\WebService;

class RouteKeyExtractor implements RouteKeyExtractorInterface {
    // TODO: string util package

    /**
     * @var string
     */
    protected $prefix;

	protected function hasPrefix(string $str) : bool {
		$prefixLength = strlen($this->prefix);
		$strPrefix = substr($str, 0, $prefixLength);
		$hasPrefix = $strPrefix === $this->prefix;

		return $hasPrefix;
	}

	protected function withoutPrefix(string $str) : string {
		if (!$this->hasPrefix($str)) {
            // TODO: throw exception
		}

		$prefixLength = strlen($this->prefix);
		$strWithoutPrefix = substr($str, $prefixLength);

		return $strWithoutPrefix;
	}

    public function setPrefix(string $prefix) {
        $this->prefix = $prefix;
    }

    public function getPrefix() : string {
        return $this->prefix;
    }

    public function extract() : string {
        $requestUri = $_SERVER['REQUEST_URI'];
		$routeWithQuery = $this->withoutPrefix($requestUri);
        // TODO: refactor
        $route = explode('?', $routeWithQuery)[0];

		return $route;
    }
}
