<?php

namespace Jyrmo\WebService;

use Jyrmo\Gateway\RequestBuilderInterface;
use Jyrmo\Router\RequestInterface;

class RequestBuilder implements RequestBuilderInterface {
    /**
     * @var RouteKeyExtractorInterface
     */
    protected $routeKeyExtractor;

    /**
     * @var MethodExtractorInterface
     */
    protected $methodExtractor;

    /**
     * @var ParamsExtractorInterface
     */
    protected $paramsExtractor;

	public function setRouteKeyExtractor(RouteKeyExtractorInterface $routeKeyExtractor) {
		$this->routeKeyExtractor = $routeKeyExtractor;
	}

	public function setMethodExtractor(MethodExtractorInterface $methodExtractor) {
		$this->methodExtractor = $methodExtractor;
	}

	public function setParamsExtractor(ParamsExtractorInterface $paramsExtractor) {
		$this->paramsExtractor = $paramsExtractor;
	}

    public function build() : RequestInterface {
        $request = new Request();
        $routeKey = $this->routeKeyExtractor->extract();
        $request->setRouteKey($routeKey);
        $method = $this->methodExtractor->extract();
        $request->setMethod($method);
        $params = $this->paramsExtractor->extract();
        $request->setParams($params);

        return $request;
    }
}
