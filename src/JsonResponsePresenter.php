<?php

namespace Jyrmo\WebService;

use Jyrmo\Gateway\ResponsePresenterInterface;
use Jyrmo\Router\ResponseInterface;

class JsonResponsePresenter implements ResponsePresenterInterface {
	// TODO: figure out bad dependency on array response.
    private function setHeader() {
        header('Content-Type: application/json');
    }

	public function present(ResponseInterface $response) {
        $this->setHeader();

		echo $response;
	}

	public function presentException(\Exception $ex) {
		// TODO: better exception printing.

        $this->setHeader();

		$arrResponse = array(
			'success' => false,
			'errorMsg' => $ex->getMessage(),
		);
		$response = json_encode($arrResponse);

		echo $response;
	}
}
