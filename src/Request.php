<?php

namespace Jyrmo\WebService;

use Jyrmo\Router\RequestInterface;

class Request implements RequestInterface {
    // TODO: more stringent method type check.
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $routeKey;

    /**
     * @var array
     */
    protected $params = array();

	public function setMethod(string $method) {
		$this->method = $method;
	}

	public function getMethod() : string {
		return $this->method;
	}

	public function setRouteKey($routeKey) {
		$this->routeKey = $routeKey;
	}

	public function getRouteKey() : string {
		return $this->routeKey;
	}

	public function setParams(array $params) {
		$this->params = $params;
	}

	public function setParam($key, $val) {
		$this->params[$key] = $val;
	}

	public function getParams() : array {
		return $this->params;
	}

	public function getParam($key) {
		return $this->params[$key];
	}
}
