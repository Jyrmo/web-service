<?php

namespace Jyrmo\WebService\ArrayResponse;

use Jyrmo\WebService\ArrayResponse;

class Success extends ArrayResponse {
    public function __construct(array $params = array()) {
        $params['success'] = true;
        parent::__construct($params);
    }
}
