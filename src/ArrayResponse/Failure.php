<?php

namespace Jyrmo\WebService\ArrayResponse;

use Jyrmo\WebService\ArrayResponse;

class Failure extends ArrayResponse {
    public function __construct(string $errorMsg = null, array $params = array()) {
        $params['success'] = false;
        if ($errorMsg) {
            $params['errorMsg'] = $errorMsg;
        }
        parent::__construct($params);
    }

    public function setErrorMsg(string $errorMsg) {
        $this->setParam('errorMsg', $errorMsg);
    }
}
