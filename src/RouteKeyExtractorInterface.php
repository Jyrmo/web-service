<?php

namespace Jyrmo\WebService;

interface RouteKeyExtractorInterface {
    // TODO: common string extractor interface etc

    public function extract() : string;
}
